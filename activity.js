// use count operator to count total number of fruits on sale

db.fruits.aggregate([
    {$match: {onSale: true}},
    {$group: {_id: "$name", total: {$count: {}}}}
]);


// count operator = count total no. of fruits with stock more than 20

db.fruits.aggregate([
    {$match: {stock: {"$gt": 20}}},
    {$group: {_id: "$name", total: {$count: {}}}}
]);


// average operator = get average price of fruits onsale per supplier

db.fruits.aggregate([
    {$match: {onSale: true}},
    {$group: {_id: "$supplier_id", total: {$avg: {$multiply: ["$price", "$stock"]}}}}
]);


// max operator - highest price of fruit per supplier

db.fruits.aggregate([
    {$group: {_id: "$supplier_id", total: {$max: "$price"}}}
]);


//  min operator - lowest price of fruit per supplier


db.fruits.aggregate([
    {$group: {_id: "$supplier_id", total: {$min: "$price"}}}
]);